<?php

namespace Drupal\imageapi_optimize_sqip\Plugin\ImageAPIOptimizeProcessor;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileRepositoryInterface;
use Drupal\imageapi_optimize\ConfigurableImageAPIOptimizeProcessorBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Psr7\Utils;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * SQIP ImageAPI Optimize Processor.
 *
 * @ImageAPIOptimizeProcessor(
 *   id = "sqip",
 *   label = @Translation("SQIP"),
 *   description = @Translation("Create an SQIP version of the image.")
 * )
 */
final class Sqip extends ConfigurableImageAPIOptimizeProcessorBase {

  /**
   * SQIP Endpoint URL.
   *
   * @var string
   */
  const SQIP_ENDPOINT = 'https://api.devweapons.com/sqip/';

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FileRepositoryInterface $file_repository,
    ClientInterface $http_client,
    ImageFactory $image_factory,
    LoggerInterface $logger,
    RequestStack $request_stack,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $image_factory);

    $this->fileRepository = $file_repository;
    $this->httpClient = $http_client;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file.repository'),
      $container->get('http_client'),
      $container->get('image.factory'),
      $container->get('logger.factory')->get('imageapi_optimize'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applyToImage($image_uri) {
    $sqip_uri = $this->determineSqipUri();
    if ($sqip_uri) {
      /** @var \Drupal\Core\Image\ImageInterface $source_image */
      $image = $this->imageFactory->get($image_uri, 'gd');

      // Limit to JPG and PNG.
      $mime_types = ['image/jpeg', 'image/png'];
      if ($image && in_array($image->getMimeType(), $mime_types)) {

        try {
          /** @var \GuzzleHttp\Psr7\Response $response */
          $response = $this->httpClient->request(
            'POST',
            self::SQIP_ENDPOINT,
            [
              'multipart' => [
                [
                  'name' => 'data',
                  'contents' => Utils::tryFopen($image_uri, 'r'),
                ],
              ],
            ]
          );

          // The exceptions below should handle all of the error codes that
          // come back from the API, but do this just to be absolutely sure.
          if ($response->getStatusCode() == 200) {
            $body = $response->getBody()->getContents();
            $data = @json_decode($body);
            if (json_last_error()) {
              $message = new TranslatableMarkup('The response from the API endpoint was malformed.');
              $this->logger->error($message);
              $this->messenger()->addError($message);
            }
            else {
              $this->fileRepository->writeData($data->sqip, $sqip_uri, FileSystemInterface::EXISTS_REPLACE);

              return TRUE;
            }
          }
          else {
            $message = new TranslatableMarkup('The API endpoint returned a non-2xx status code.');
            $this->logger->error($message);
            $this->messenger()->addError($message);
          }
        }

        // Handle 400 errors.
        catch (ClientException $ex) {
          $message = new TranslatableMarkup('The API endpoint returned a 4xx error code.');
          $this->logger->error($message);
          $this->messenger()->addError($message);
        }

        // Handle 500 errors.
        catch (ServerException $ex) {
          $message = new TranslatableMarkup('The API endpoint returned a 5xx error code.');
          $this->logger->error($message);
          $this->messenger()->addError($message);
        }

        // Connection errors.
        catch (ConnectException $ex) {
          $message = new TranslatableMarkup('The connection to the API endpoint could not be made.');
          $this->logger->error($message);
          $this->messenger()->addError($message);
        }

        // Too many redirects.
        catch (TooManyRedirectsException $ex) {
          $message = new TranslatableMarkup('The API endpoint redirected too many times.');
          $this->logger->error($message);
          $this->messenger()->addError($message);
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('There are no configuration options for this processor.'),
    ];
    return $form;
  }

  /**
   * Determine SQIP URI.
   */
  protected function determineSqipUri() {

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $this->requestStack->getCurrentRequest();
    $image_path = $request->getRequestUri();

    // Remove the query string.
    $image_path = strtok($image_path, '?');

    // Extract details from the path. This will always be some kind of image
    // style URL, so safe assumptions can be made:
    //
    // ^/(.*+?) = public or private path prefix. This can be discarded.
    // /styles
    // /([^/]+) = image style name.
    // /([^/]+) = file system name.
    // /(.*)$ = the rest of the file name.
    if (preg_match('#^/(.+?)/styles/([^/]+)/([^/]+)/(.*)$#', $image_path, $matches)) {
      $image_path = $matches[3] . '://styles/' . $matches[2] . '/' . $matches[3] . '/' . $matches[4] . '.svg';

      return $image_path;
    }

    return FALSE;
  }

}
