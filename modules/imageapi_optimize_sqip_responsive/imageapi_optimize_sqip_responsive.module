<?php

/**
 * @file
 * Hooks and helper functions for the Image Optimize SQIP Responsive module.
 */

use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * Implements hook_help().
 */
function imageapi_optimize_sqip_responsive_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.imageapi_optimize_sqip_responsive':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}
/**
 * Implements template_preprocess_responsive_image().
 */
function imageapi_optimize_sqip_responsive_preprocess_responsive_image(&$variables) {
  $has_sqip = FALSE;

  /** @var \Drupal\responsive_image\Entity\ResponsiveImageStyle $responsive_image_style */
  $responsive_image_style = \Drupal::entityTypeManager()
    ->getStorage('responsive_image_style')
    ->load($variables['responsive_image_style_id']);
  $image_styles = \Drupal::entityTypeManager()
    ->getStorage('image_style')
    ->loadMultiple($responsive_image_style->getImageStyleIds());
  /** @var \Drupal\image\Entity\ImageStyle $image_style */
  foreach ($image_styles as $image_style) {
    if ($image_style->hasPipeline()) {
      /** @var \Drupal\imageapi_optimize\ImageAPIOptimizeProcessorInterface $processor */
      foreach ($image_style->getPipelineEntity()->getProcessors() as $processor) {

        // Make sure the responsive image style has an image style that uses
        // the SQIP processor.
        if ($processor->getPluginId() == 'sqip') {
          $has_sqip = TRUE;
          break 2;
        }
      }
    }
  }

  if ($has_sqip) {
    /** @var \Drupal\image\Entity\ImageStyle $image_style */
    $image_style = \Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->load($responsive_image_style->getFallbackImageStyle());

    // The uri key needs to have the fallback image style applied before the
    // svg extension is added.
    $derivative_uri = $image_style->buildUri($variables['uri']);
    $sqip = $derivative_uri . '.svg';

    // Swap out the SQIP and URI.
    if (file_exists($sqip)) {
      $sqip_data = base64_encode(file_get_contents($sqip));
      $variables['img_element']['#attributes']['data-src'] = $variables['img_element']['#uri'];
      $variables['img_element']['#uri'] = 'data:image/svg+xml;base64,' . $sqip_data;
      $variables['#attached']['library'][] = 'imageapi_optimize_sqip_responsive/lazyload';
      $variables['img_element']['#attributes']['class'][] = 'lazy';
    }
  }
}
