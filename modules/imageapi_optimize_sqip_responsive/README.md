# ImageAPI Optimize SQIP Responsive

The Image Optimize SQIP module provides and integration with the Responsive Images core module and lazy loading functionality.

## Table of contents

* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

## Requirements

This module requires the [ImageAPI Optimize SQIP](https://drupal.org/project/imageapi_optimize_sqip) module and the core Responsive Images module.

## Recommended modules

* [Markdown filter](https://www.drupal.org/project/markdown): When enabled, display of the project's README.md help will be rendered with markdown.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

None. The SQIP functionality will automatically apply to Responsive Image image styles that are configured to use image styles that use the SQIP processor.
