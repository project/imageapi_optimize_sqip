# ImageAPI Optimize SQIP

The Image Optimize SQIP module provides an SQIP (SVG low quality image placeholder) processor for the Image Optimize pipeline system.

For a full description of the module, visit the [project page](https://drupal.org/project/imageapi_optimize_sqip)
Submit bug reports and feature suggestions, or track changes in the [issue queue](https://drupal.org/project/issues/imageapi_optimize_sqip)

## Table of contents

* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

## Requirements

This module requires the [ImageAPI Optimize](https://drupal.org/project/imageapi_optimize) module and the core File module.

## Recommended modules

* [Markdown filter](https://www.drupal.org/project/markdown): When enabled, display of the project's README.md help will be rendered with markdown.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Visit the Image Optimize pipelines configuration page at Administration > Configuration > Media > Image Optimize pipelines.
1. Add a new pipeline or edit an existing one.
1. Add an SQIP processor.

## Additional Tasks

Install the bundled ImageAPI Optimize SQIP Responsive module to integrate with the core Responsive Image module which allows for responsive images using the <picture> tag and enabled lazy loading.

OR

Manually use the generated SQIP in the theme.